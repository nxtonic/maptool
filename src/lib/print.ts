import type { Map } from "ol";
import { jsPDF } from "jspdf";

const dims = {
  a0: [1189, 841],
  a1: [841, 594],
  a2: [594, 420],
  a3: [420, 297],
  a4: [297, 210],
  a5: [210, 148],
};

export interface exportOptions {
  pageSize: string;
  resolution: number;
  rotation: string;
}

export const exportToPDF = (map: Map, opts: exportOptions, cb: Function) => {
  const dimensions = dims[opts.pageSize];
  const size = map.getSize();
  const viewResolution = map.getView().getResolution();

  const x = opts.rotation == "l" ? dimensions[0] : dimensions[1];
  const y = opts.rotation == "l" ? dimensions[1] : dimensions[0];

  const width = Math.round((x * opts.resolution) / 25.4);
  const height = Math.round((y * opts.resolution) / 25.4);

  map.once("rendercomplete", () => {
    const mapCanvas = document.createElement("canvas");
    const mapContext = mapCanvas.getContext("2d");

    mapCanvas.width = width;
    mapCanvas.height = height;

    Array.prototype.forEach.call(
      document.querySelectorAll(".ol-layer canvas"),
      (canvas) => {
        if (canvas.width > 0) {
          const opacity = canvas.parentNode.style.opacity;
          const transform = canvas.style.transform;

          mapContext.globalAlpha = opacity === "" ? 1 : Number(opacity);

          // Get the transform parameters from the style's transform matrix
          const matrix = transform
            .match(/^matrix\(([^\(]*)\)$/)[1]
            .split(",")
            .map(Number);

          // Apply the transform to the export map context
          CanvasRenderingContext2D.prototype.setTransform.apply(
            mapContext,
            matrix
          );

          mapContext.drawImage(canvas, 0, 0);
        }
      }
    );

    mapContext.globalAlpha = 1;

    opts.rotation == "l"
      ? mapContext.setTransform(1, 0, 0, 1, 0, 0)
      : mapContext.setTransform(1, 0, 0, 1, 0, 0);

    const pdf = new jsPDF({
      orientation: opts.rotation,
      format: opts.pageSize,
    });

    pdf.addImage(mapCanvas.toDataURL("image/jpeg"), "JPEG", 0, 0, x, y);

    pdf.save("map.pdf");

    // Reset original map size
    map.setSize(size);
    map.getView().setResolution(viewResolution);

    cb();
  });

  // Set print size
  const printSize = [width, height];
  const scaling = Math.min(width / size[0], height / size[1]);

  map.setSize(printSize);
  map.getView().setResolution(viewResolution / scaling);
};
