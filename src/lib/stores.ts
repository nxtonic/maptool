import { writable } from "svelte/store";

import Map from "ol/Map";

export const map = writable(new Map());
export const config = writable({
  files: [],
  rendering: {
    resolution: 300,
    pageSize: "a3",
    rotation: "portrait",
  },
});
