import { Style, Fill, Stroke, Text, Circle } from "ol/style";

const iconSize = 5;

const turnpointLabel = (feature, res) => {
  const rex = /^(\d+)([\d\w]+)/;
  const match = feature.get("name").match(rex);

  if (!match) {
    return;
  }

  let style;

  switch (res) {
    case res < 100:
      style = [
        "condensed 800 32px/24px Roboto Mono",
        "condensed 800 18px/16px Roboto Mono",
      ];
      break;

    case res < 200:
      style = [
        "condensed 800 22px/16px Roboto Mono",
        "condensed 800 18px/16px Roboto Mono",
      ];
      break;

    default:
      style = [
        "condensed 800 32px/24px Roboto Mono",
        "condensed 800 18px/16px Roboto Mono",
      ];
      break;
  }

  if (match.length == 3) {
    return [match[1], style[0], "\n", "0px Roboto", match[2], style[1]];
  }
};

export const Styles = {
  none: (feature, resolution) => {
    return new Style();
  },
  nolabels: (feature, resolution) => {
    return new Style({
      stroke: new Stroke({
        color: "magenta",
        width: 2,
      }),
      image: new Circle({
        fill: new Fill({ color: "magenta" }),
        // stroke: new Stroke({ color: "white", width: 1 }),
        radius: iconSize,
      }),
    });
  },

  simple: (feature, resolution) => {
    return new Style({
      /* fill: new Fill({
        color: "rgba(0,0,0,0)",
      }), */
      stroke: new Stroke({
        color: "magenta",
        width: 2,
      }),
      text: new Text({
        text: [feature.get("name"), "condensed 800 22px/16px Roboto Mono"],
        // rotation: -0.61,
        textAlign: "center",
        justify: "center",
        textBaseline: "middle",
        offsetX: iconSize / 4,
        /* backgroundFill: new Fill({
          color: "rgba(255,255,255,0.8)",
        }), */
      }),
      image: new Circle({
        fill: new Fill({ color: "magenta" }),
        // stroke: new Stroke({ color: "white", width: 1 }),
        radius: iconSize,
      }),
    });
  },

  turnpoints: (feature, resolution) => {
    return new Style({
      stroke: new Stroke({
        color: "magenta",
        width: 2,
      }),
      text: new Text({
        font: "22px Roboto Mono",
        text: turnpointLabel(feature, resolution),
        textAlign: "left",
        justify: "right",
        textBaseline: "top",
        offsetX: iconSize / 4,
      }),
      image: new Circle({
        fill: new Fill({ color: "magenta" }),
        // stroke: new Stroke({ color: "white", width: 1 }),
        radius: iconSize,
      }),
    });
  },

  verso: (feature, resolution) => {
    if (
      feature.getGeometry().getType() == "LineString" &&
      feature.get("name").includes("Circle")
    ) {
      return new Style({
        stroke: new Stroke({
          color: "red",
          width: 3,
        }),
        text: new Text({
          placement: "line",
          text: feature.get("name"),
          font: "bold 42px/36px Roboto Mono",
          offsetY: 36,
          textAlign: "left",
          textBaseline: "ideographic",
        }),
      });
    } else if (
      feature.getGeometry().getType() == "LineString" &&
      feature.get("name").includes("Track")
    ) {
      return new Style({
        stroke: new Stroke({
          color: "#00ff00",
          width: 5,
        }),
        text: new Text({
          placement: "line",
          text: feature.get("name"),
          font: "bold 32px/24px Roboto Mono",
          offsetY: 16,
          textAlign: "center",
          textBaseline: "ideographic",
        }),
      });
    } else if (feature.get("name").includes("YNRM")) {
      return new Style({
        fill: new Fill({
          color: "rgba(0,0,0,0)",
        }),
        stroke: new Stroke({
          color: "purple",
          width: 2,
        }),
        text: new Text({
          font: "32px Roboto Mono",
          text: [
            feature.get("name").substring(0, 3),
            "condensed 800 64px/48px Roboto Mono",
            "\n",
            "0px Roboto",
            feature.get("name").substring(3, 100),
            "condensed 800 36px/32px Roboto Mono",
          ],

          // rotation: -0.61,
          textAlign: "left",
          justify: "right",
          textBaseline: "top",
          offsetX: iconSize / 4,
          /* backgroundFill: new Fill({
          color: "rgba(255,255,255,0.8)",
        }), */
        }),
        image: new Circle({
          fill: new Fill({ color: "red" }),
          // stroke: new Stroke({ color: "white", width: 1 }),
          radius: iconSize,
        }),
      });
    } else if (feature.get("name").match(/^\d{3}.+/g)) {
      return new Style({
        fill: new Fill({
          color: "rgba(0,0,0,0)",
        }),
        stroke: new Stroke({
          color: "purple",
          width: 2,
        }),
        text: new Text({
          font: "22px Roboto Mono",
          text: [
            feature.get("name").substring(0, 3),
            "condensed 800 64px/48px Roboto Mono",
            "\n",
            "0px Roboto",
            feature.get("name").substring(3, 100),
            "condensed 800 36px/32px Roboto Mono",
          ],

          // rotation: -0.61,
          textAlign: "left",
          justify: "right",
          textBaseline: "top",
          offsetX: iconSize / 4,
          /* backgroundFill: new Fill({
          color: "rgba(255,255,255,0.8)",
        }), */
        }),
        image: new Circle({
          fill: new Fill({ color: "magenta" }),
          // stroke: new Stroke({ color: "white", width: 1 }),
          radius: iconSize,
        }),
      });
    }
  },
};
