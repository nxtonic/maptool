import "./app.css";
import "ol/ol.css";
import App from "./App.svelte";
import "@fontsource/roboto";
import "@fontsource/roboto-mono";

const app = new App({
  target: document.getElementById("app"),
});

export default app;
