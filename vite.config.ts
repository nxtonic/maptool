import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

import { execSync } from "child_process";
import "process";

export default () => {
  process.env.VITE_BUILD_DATE = String(new Date().toISOString());
  process.env.VITE_BUILD_BRANCH = execSync(
    "git symbolic-ref --short HEAD 2> /dev/null || echo 'BUILD'"
  )
    .toString()
    .trimEnd();
  process.env.VITE_COMMIT_ABBREV = execSync(
    "git describe --long --always --dirty"
  )
    .toString()
    .trimEnd();
  process.env.VITE_COMMIT_DATE = execSync("git log -1 --format=%cI")
    .toString()
    .trimEnd();

  return defineConfig({
    plugins: [svelte()],
    base: "/maptool/",
  });
};
